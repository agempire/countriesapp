//
//  CountryInfoRouter.swift
//  CountriesApp
//
//  Created by Vasily Agafonov on 30/09/2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import UIKit.UIViewController

protocol CountryInfoRouter {
    func showInfo(for country: CountriesListViewModel.Country)
}

final class MinimalCountryInfoRouter: CountryInfoRouter {
    
    unowned let viewController: UIViewController
    
    init(context: UIViewController) {
        viewController = context
    }
    
    func showInfo(for country: CountriesListViewModel.Country) {
        guard let vc = viewController.storyboard?.instantiateViewController(withIdentifier: CountryInfoViewController.storyboardIdentifier) as? CountryInfoViewController else { return }
        
        vc.viewModel = CountryInfoViewModel(country: country)
        
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
}
