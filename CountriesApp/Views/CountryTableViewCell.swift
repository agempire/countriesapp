//
//  CountryTableViewCell.swift
//  CountriesApp
//
//  Created by Vasily Agafonov on 29/09/2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import UIKit

final class CountryTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = "CountryTableViewCell"
    
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var countryPopulationLabel: UILabel!
}
