//
//  CountriesListViewModel.swift
//  CountriesApp
//
//  Created by Vasily Agafonov on 30/09/2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

final class CountriesListViewModel {
    
    struct Country: Codable {
        let name: String
        let population: Int
    }
    
    private let networkManager = NetworkManager()
    
    private let disposeBag = DisposeBag()
    
    let isPulling = BehaviorRelay(value: false)
    
    let isFetching = BehaviorRelay(value: false)
    
    let countries: BehaviorRelay<[Country]> = BehaviorRelay(value: [])
    
    let error: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    
    init(onPullToRefresh: Observable<Void>, onTryToRecover: Observable<Void>, onCountrySelected: Observable<Country>, router: CountryInfoRouter) {
        onTryToRecover
            .subscribe(onNext: { self.fetchCountries() })
            .disposed(by: disposeBag)
        
        onPullToRefresh
            .subscribe(onNext: { self.fetchCountries(pulling: true) })
            .disposed(by: disposeBag)
        
        onCountrySelected
            .subscribe(onNext: { (country) in router.showInfo(for: country) })
            .disposed(by: disposeBag)
        
        fetchCountries()
    }
    
    private func fetchCountries(pulling: Bool = false) {
        notifyLoadingSystem(relay: pulling ? isPulling : isFetching, with: true)
        
        networkManager
            .request(endpoint: .list)
            .map([Country].self)
            .do(onNext: { [weak self] _ in
                guard let strongSelf = self else { return }
                
                strongSelf.notifyLoadingSystem(relay: pulling ? strongSelf.isPulling : strongSelf.isFetching, with: false)
            })
            .subscribe(
                onNext: { [weak self] (countries) in
                    self?.countries.accept(countries)
                },
                onError: { [weak self] (error) in
                    self?.error.accept(error.localizedDescription)
                }
            )
            .disposed(by: disposeBag)
    }
    
    private func notifyLoadingSystem(relay: BehaviorRelay<Bool>, with value: Bool) {
        relay.accept(value)
    }
}
