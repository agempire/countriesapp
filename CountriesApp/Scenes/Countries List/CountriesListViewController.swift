//
//  CountriesListViewController.swift
//  CountriesApp
//
//  Created by Vasily Agafonov on 29/09/2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa

final class CountriesListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var errorView: ErrorView!
    
    lazy var viewModel: CountriesListViewModel = {
        guard let refreshControl = tableView.refreshControl
            else { fatalError("No refresh control") }
        
        return CountriesListViewModel(
            onPullToRefresh: refreshControl.rx.controlEvent(.valueChanged).asObservable(),
            onTryToRecover: errorView.button.rx.tap.asObservable(),
            onCountrySelected: tableView.rx.modelSelected(CountriesListViewModel.Country.self).asObservable(),
            router: MinimalCountryInfoRouter(context: self)
        )
    }()
    
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refreshControl = UIRefreshControl()
        tableView.refreshControl = refreshControl
        tableView.tableFooterView = UIView()
        
        viewModel.isPulling
            .bind(to: refreshControl.rx.isRefreshing)
            .disposed(by: disposeBag)
        
        viewModel.countries.asObservable()
            .do(onNext: { [weak self] _ in
                guard let strongSelf = self else { return }
                
                strongSelf.view.bringSubviewToFront(strongSelf.tableView)
            })
            .bind(to: tableView.rx.items(cellIdentifier: CountryTableViewCell.reuseIdentifier, cellType: CountryTableViewCell.self)) { (index, model, cell) in
                cell.countryNameLabel.text = model.name
                cell.countryPopulationLabel.text = "\(model.population)"
            }
            .disposed(by: disposeBag)
        
        viewModel.error.asObservable()
            .filter({ $0 != nil })
            .do(onNext: { [weak self] _ in
                guard let strongSelf = self else { return }
                
                strongSelf.view.bringSubviewToFront(strongSelf.errorView)
            })
            .subscribe(onNext: { [weak self] (error) in
                self?.errorView.errorLabel.text = error
            })
            .disposed(by: disposeBag)
        
        viewModel.isFetching.asObservable()
            .subscribe(onNext: { [weak self] (isFetching) in
                guard let strongSelf = self, isFetching else { return }
                
                strongSelf.view.bringSubviewToFront(strongSelf.loaderView)
            })
            .disposed(by: disposeBag)
    }
}
