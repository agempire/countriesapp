//
//  CountryInfoViewController.swift
//  CountriesApp
//
//  Created by Vasily Agafonov on 30/09/2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa

final class CountryInfoViewController: UIViewController {
    
    static let storyboardIdentifier = "CountryInfoViewController"
    
    @IBOutlet weak var countryBriefLabel: UILabel!
    @IBOutlet weak var countryNeighboursLabel: UILabel!
    @IBOutlet weak var countryCurrenciesLabel: UILabel!
    
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var errorView: ErrorView!
    
    var viewModel: CountryInfoViewModel!
    
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.isLoading.map({ !$0 }).bind(to: loaderView.rx.isHidden).disposed(by: disposeBag)
        
        viewModel.title.bind(to: navigationItem.rx.title).disposed(by: disposeBag)
        viewModel.brief.bind(to: countryBriefLabel.rx.text).disposed(by: disposeBag)
        viewModel.neighbours.bind(to: countryNeighboursLabel.rx.text).disposed(by: disposeBag)
        viewModel.currencies.bind(to: countryCurrenciesLabel.rx.text).disposed(by: disposeBag)
    }
}
