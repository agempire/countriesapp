//
//  CountryInfoViewModel.swift
//  CountriesApp
//
//  Created by Vasily Agafonov on 30/09/2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

final class CountryInfoViewModel {
    
    struct CountryInfo: Codable {
        
        struct Currency: Codable {
            let code: String
            let name: String
            let symbol: String
        }
        
        let capital: String
        let region: String
        let subregion: String
        let population: Int
        let borders: [String]
        let currencies: [Currency]
        
        var brief: String {
            return "Country in \(region) (\(subregion)). Capital city is \(capital). Total population - \(population)."
        }
        
        var localizedCurrencies: String {
            return currencies.map({ "\($0.symbol) (\($0.name))" }).joined(separator: ", ")
        }
    }
    
    struct CountryName: Codable {
        let name: String
    }
    
    private let networkManager = NetworkManager()
    
    private let disposeBag = DisposeBag()
    
    let isLoading: BehaviorRelay<Bool> = BehaviorRelay(value: true)
    
    let brief: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    
    let neighbours: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    
    let currencies: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    
    let isFailed: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    
    let title: Observable<String>
    
    init(country: CountriesListViewModel.Country) {
        title = Observable.just(country.name)
        
        fetchInfo(for: country)
    }
    
    private func fetchInfo(for country: CountriesListViewModel.Country) {
        isLoading.accept(true)
        
        networkManager
            .request(endpoint: .name(country.name))
            .map([CountryInfo].self)
            .subscribe(
                onNext: { (infos) in
                    print(infos)
                    
                    guard let info = infos.first else { return }
                    
                    self.brief.accept(info.brief)
                    self.currencies.accept(info.localizedCurrencies)
                    
                    guard info.borders.count > 0 else {
                        self.isLoading.accept(false)
                        return
                    }
                    
                    let neighbours = info.borders.map {
                        self.networkManager.request(endpoint: .alpha($0)).map(CountryName.self)
                    }
                    
                    Observable.combineLatest(neighbours)
                        .map({ (array) -> String in
                            array.map({ $0.name }).joined(separator: ", ")
                        })
                        .do(onNext: { _ in
                            self.isLoading.accept(false)
                        })
                        .subscribe(
                            onNext: { (neigh) in
                                self.neighbours.accept(neigh)
                            },
                            onError: { (error) in
                                self.isFailed.accept(error.localizedDescription)
                            }
                        )
                        .disposed(by: self.disposeBag)
                },
                onError: { (error) in
                    self.isFailed.accept(error.localizedDescription)
                }
            )
            .disposed(by: disposeBag)
    }
}
