//
//  CountriesAPI.swift
//  CountriesApp
//
//  Created by Vasily Agafonov on 30/09/2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import Foundation
import Moya

enum CountriesAPI {
    case list
    case name(String)
    case alpha(String)
}

extension CountriesAPI: TargetType {
    
    var baseURL: URL {
        guard let url = URL(string: "https://restcountries.eu/rest/v2")
            else { fatalError("Could not create countries endpoint url!") }
        
        return url
    }
    
    var path: String {
        switch self {
        case .list:
            return "/all"
        case .name(let name):
            return "/name/\(name)"
        case .alpha(let name):
            return "/alpha/\(name)"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var task: Task {
        switch self {
        case .list:
            return .requestParameters(parameters: ["fields": "name;population"], encoding: URLEncoding.default)
        case .name:
            return .requestPlain
        case .alpha:
            return .requestParameters(parameters: ["fields": "name"], encoding: URLEncoding.default)
        }
    }
    
    var sampleData: Data {
        return Data()
    }
}
