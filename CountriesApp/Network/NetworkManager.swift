//
//  NetworkManager.swift
//  CountriesApp
//
//  Created by Vasily Agafonov on 30/09/2018.
//  Copyright © 2018 Vasily Agafonov. All rights reserved.
//

import Foundation

import RxSwift
import Moya

final class NetworkManager {
    
    private let provider: MoyaProvider<CountriesAPI> = MoyaProvider()
    
    func request(endpoint: CountriesAPI) -> Observable<Response> {
        return provider.rx.request(endpoint).asObservable()
    }
}
